module.exports = {
  parser: '@babel/eslint-parser',
  settings: {
    react: {
      version: 'detect'
    }
  },
  env: {
    browser: true,
    es2021: true
  },
  extends: ['eslint:recommended', 'plugin:react/recommended', 'standard', 'plugin:react/jsx-runtime'],
  parserOptions: {
    sourceType: 'module'
  },
  rules: {
    indent: ['error', 2]
  }
}
