import { useReducer } from 'react'

import './App.css'
import reducer, { types } from './helpers/inventoryCountReducer'
import InventoryItem from './components/Pages/InventoryItem/InventoryItem'
import initialState, { emptyState } from './helpers/initialState'

function App () {
  const [state, dispatch] = useReducer(reducer, initialState)
  const handleDispatch = (type, payload) => {
    dispatch({ type, payload })
  }
  const INITIAL_ID = 1
  const getNextID = state.length ? (state[state.length - 1].id + 1) : INITIAL_ID

  return (
    <div className="inventory__container">
      {!!state.length && state.map((inventoryItem) => (
        <InventoryItem
          key={inventoryItem.id}
          handleDispatch={handleDispatch}
          inventoryItem={inventoryItem}
        />
      ))}

      <div className='btn__container'>
        <button className='btn__add-item' onClick={() => handleDispatch(types.SET_NEW_ITEM, { ...emptyState, id: getNextID }
        )}>Add new item</button>
      </div>
    </div>
  )
}

export default App
