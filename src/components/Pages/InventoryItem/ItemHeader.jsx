import { PropTypes } from 'prop-types'

function ItemHeader ({ inventoryItem }) {
  const setQuantityMeasurement = () => {
    if (inventoryItem.quantity > 1) return 'pieces'
    if (inventoryItem.quantity === 1) return 'piece'
    return 'No stock left'
  }

  return (
    <>
      <h1 className="text--black">{inventoryItem.name}</h1>
      <h2 className={`${inventoryItem.quantity ? 'text--black' : 'text--red'}`}>
        {!!inventoryItem.quantity && inventoryItem.quantity}{' '}
        {setQuantityMeasurement()}
      </h2>
    </>
  )
}

ItemHeader.propTypes = {
  inventoryItem: PropTypes.object
}

export default ItemHeader
