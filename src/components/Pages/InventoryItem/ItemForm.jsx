import { PropTypes } from 'prop-types'

import Button from '../../Utils/Button'
import { types } from '../../../helpers/inventoryCountReducer'
import styles from './ItemForm.module.css'

function ItemForm ({ handleDispatch, inventoryItem }) {
  const handleKeyDown = ({ key }) => {
    if (key === 'Enter') {
      handleDispatch(types.SET_ITEM, { id: inventoryItem.id })
    }
  }

  return (
    <div className="App">
      <div className="item-form card">
        <div>
          <Button
            handleOnClick={() => {
              handleDispatch(types.SUBTRACT_QUANTITY, { id: inventoryItem.id })
            }}
            buttonText={'-'}
          />{' '}
          <Button
            handleOnClick={() =>
              handleDispatch(types.ADD_QUANTITY, { id: inventoryItem.id })
            }
            buttonText={'+'}
          />
        </div>
        <div className={styles['item-form__container']}>
          <input
            className={styles['item-form__input-name']}
            value={inventoryItem.temporaryName}
            onKeyDown={handleKeyDown}
            onChange={(e) => {
              handleDispatch(types.HANDLE_NAME_CHANGE, {
                temporaryName: e.target.value,
                id: inventoryItem.id
              })
            }}
          />{' '}

          <input
            className={styles['item-form__input-quantity']}
            value={inventoryItem.temporaryQuantity}
            type="number"
            onKeyDown={handleKeyDown}
            onChange={(e) => {
              if (!isNaN(e.target.value)) {
                handleDispatch(types.HANDLE_QUANTITY_CHANGE, {
                  temporaryQuantity: parseInt(e.target.value),
                  id: inventoryItem.id
                })
              }
            }}
          />{' '}
        </div>
        <div>
          <Button
            handleOnClick={() => handleDispatch(types.SET_ITEM, { id: inventoryItem.id })
            }
            buttonText={'Save'}
          />{' '}
        </div>
      </div>
    </div>
  )
}

ItemForm.propTypes = {
  handleDispatch: PropTypes.func,
  inventoryItem: PropTypes.object
}

export default ItemForm
