import { PropTypes } from 'prop-types'

import ItemForm from './ItemForm'
import ItemHeader from './ItemHeader'
import styles from './InventoryItem.module.css'
import Button from '../../Utils/Button'
import { types } from '../../../helpers/inventoryCountReducer'

function InventoryItem ({ handleDispatch, inventoryItem }) {
  return (
    <>
      <div className={styles.inventory__item}>
        <div className={styles['inventory__btn--delete']}>
          <Button buttonText={'X'} handleOnClick={() => handleDispatch(types.DELETE_ITEM, { id: inventoryItem.id })}
          />
        </div>
        <ItemHeader inventoryItem={inventoryItem} />
        <ItemForm
          handleDispatch={handleDispatch}
          inventoryItem={inventoryItem}
        />
      </div>
    </>
  )
}

InventoryItem.propTypes = {
  handleDispatch: PropTypes.func,
  inventoryItem: PropTypes.object
}

export default InventoryItem
