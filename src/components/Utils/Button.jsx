import { PropTypes } from 'prop-types'

function Button ({ handleOnClick, buttonText }) {
  return (
    <>
      <button onClick={handleOnClick}> {buttonText}</button>
    </>
  )
}

Button.propTypes = {
  handleOnClick: PropTypes.func,
  buttonText: PropTypes.string
}

export default Button
