export const types = {
  ADD_QUANTITY: 'ADD_QUANTITY',
  SUBTRACT_QUANTITY: 'SUBTRACT_QUANTITY',
  HANDLE_QUANTITY_CHANGE: 'HANDLE_QUANTITY_CHANGE',
  HANDLE_NAME_CHANGE: 'HANDLE_NAME_CHANGE',
  SET_ITEM: 'SET_ITEM',
  SET_NEW_ITEM: 'SET_NEW_ITEM',
  DELETE_ITEM: 'DELETE_ITEM'
}

const reducer = (state, { type, payload }) => {
  const getInventoryById = state.find(
    (singleItem) => singleItem.id === payload.id
  )

  const updateState = ({ id }, inventoryItemChanges) => {
    const updatedState = state.map((inventoryItem) => {
      if (inventoryItem.id === id) {
        return {
          ...inventoryItem,
          ...inventoryItemChanges
        }
      }
      return inventoryItem
    })

    return updatedState
  }

  switch (type) {
  case types.ADD_QUANTITY:
    return updateState(payload, {
      quantity: getInventoryById.quantity + 1,
      temporaryQuantity: getInventoryById.temporaryQuantity + 1
    })
  case types.SUBTRACT_QUANTITY:
    if (getInventoryById.quantity) {
      return updateState(payload, {
        quantity: getInventoryById.quantity - 1,
        temporaryQuantity: getInventoryById.temporaryQuantity - 1
      })
    }
    break
  case types.HANDLE_QUANTITY_CHANGE:
    return updateState(payload, {
      temporaryQuantity: payload.temporaryQuantity
    })
  case types.HANDLE_NAME_CHANGE:
    return updateState(payload, {
      temporaryName: payload.temporaryName
    })
  case types.SET_ITEM:
    return updateState(payload, {
      quantity: getInventoryById.temporaryQuantity,
      name: getInventoryById.temporaryName
    })
  case types.SET_NEW_ITEM:
    return [...state, payload]
  case types.DELETE_ITEM:
    return state.filter(item => item.id !== payload.id)
  default:
    throw new Error()
  }
}

export default reducer
