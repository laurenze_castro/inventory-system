
export const emptyState = {
  id: 0,
  name: '...',
  type: 1,
  quantity: 1,
  temporaryQuantity: 1,
  temporaryName: '...'
}

export default [
  {
    id: 1,
    name: 'Paper',
    type: 1,
    quantity: 1,
    temporaryQuantity: 1,
    temporaryName: 'Paper'
  },
  {
    id: 2,
    name: 'Pen',
    type: 1,
    quantity: 1,
    temporaryQuantity: 1,
    temporaryName: 'Pen'
  }
]
